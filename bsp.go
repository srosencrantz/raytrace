/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package raytrace

import (
	"fmt"
	"math"
	"sort"

	"bitbucket.org/srosencrantz/vect3d"
)

// DirectionType specifies the X, Y, or Z directions
type DirectionType int

// Enums for DirectionType
const (
	X DirectionType = 0
	Y DirectionType = 1
	Z DirectionType = 2
)

var dirList = []DirectionType{X, Y, Z}

// Next gives you the next direction
func (dir DirectionType) next() (nextDir DirectionType) {
	switch dir {
	case X:
		nextDir = Y
	case Y:
		nextDir = Z
	case Z:
		nextDir = X
	}
	return nextDir
}

type nodeRay struct {
	node *BspNode
	ray  *Ray
}

func newNodeRay(n *BspNode, r *Ray) *nodeRay {
	return &nodeRay{node: n, ray: r}
}

type nodeRayList []*nodeRay

// BspNode represents a node of a BSP of facets
type BspNode struct {
	objs     []*Tri
	layer    int
	dir      DirectionType
	min, max [3]float64
	splitVal float64
	greater  *BspNode
	lesser   *BspNode
}

type bspNodeList []*BspNode

func newBspNode(o []*Tri, l int, min, max [3]float64) *BspNode {
	return &BspNode{objs: o, layer: l, min: min, max: max}
}

// Sprint prints a tree
func (node *BspNode) Sprint() (output string) {
	nodeList := bspNodeList{node}
	output = ""
	for i := 0; i < len(nodeList); i++ {
		newStr, newNodes := nodeList[i].sprint()
		nodeList = append(nodeList, newNodes...)
		output = fmt.Sprintf("%s%s", output, newStr)
	}
	return output
}

func (node *BspNode) sprint() (output string, bspNL []*BspNode) {
	output = fmt.Sprintf("Layer: %d, Num Objs: %d, Dir: %v, SplitValue: %f, Min: %v, Max %v\n", node.layer, len(node.objs), node.dir, node.splitVal, node.min, node.max)
	if node.greater != nil {
		bspNL = append(bspNL, node.greater)
	}
	if node.lesser != nil {
		bspNL = append(bspNL, node.lesser)
	}
	return output, bspNL
}

// CreateBspTree creates a BSP tree given a list of facets
func CreateBspTree(minNodeSize float64, objs TriList) (rootNode *BspNode, err error) {
	max := [3]float64{-math.MaxFloat64, -math.MaxFloat64, -math.MaxFloat64}
	min := [3]float64{math.MaxFloat64, math.MaxFloat64, math.MaxFloat64}
	for _, obj := range objs {
		for i := 0; i < 3; i++ {
			min[i] = math.Min(min[i], obj.min[i])
			max[i] = math.Max(max[i], obj.max[i])
		}
	}
	rootNode = newBspNode(objs, 0, min, max)
	nodeList := []*BspNode{rootNode}
	for i := 0; i < len(nodeList); i++ {
		nodeList = append(nodeList, nodeList[i].setup(minNodeSize)...)
	}
	return rootNode, nil
}

func (node *BspNode) setup(minNodeSize float64) (bspNL []*BspNode) {
	// if the number of objects is less than maxObjs or this is the max node layer then this is a leaf node, so return.
	size := vect3d.Subtract(node.max, node.min)
	node.dir = X
	if size[1] > size[0] && size[1] > size[2] {
		node.dir = Y
	}
	if size[2] > size[0] && size[2] > size[1] {
		node.dir = Z
	}
	maxsize := size[node.dir]

	if (len(node.objs) < 1) || (maxsize < minNodeSize) {
		node.splitVal = (node.min[node.dir] + node.max[node.dir]) / float64(2.0)
		return
	}

	node.splitVal = (node.min[node.dir] + node.max[node.dir]) / float64(2.0)

	// determine which objects go where, if the object is located on:
	//  - both sides of splitVal, the send copies to the both  child nodes
	//  - the lesser side of splitVal only, then the facet goes to the Lesser child node
	//  - the greater side of splitVal only, then the facet goes to the greater child node
	greaterObjs := make([]*Tri, 0)
	lesserObjs := make([]*Tri, 0)
	for _, obj := range node.objs {
		if obj.MinLoc(node.dir) < node.splitVal && obj.MaxLoc(node.dir) < node.splitVal {
			lesserObjs = append(lesserObjs, obj)
		} else if obj.MinLoc(node.dir) > node.splitVal && obj.MaxLoc(node.dir) > node.splitVal {
			greaterObjs = append(greaterObjs, obj)
		} else {
			lesserObjs = append(lesserObjs, obj)
			greaterObjs = append(greaterObjs, obj)
		}
	}

	// deal with this nodes objs
	node.objs = nil

	// create the lesser and greater nodes
	if len(lesserObjs) > 0 {
		lessermax := node.max
		lessermax[node.dir] = node.splitVal
		node.lesser = newBspNode(lesserObjs, node.layer+1, node.min, lessermax)
		bspNL = append(bspNL, node.lesser)
	}
	if len(greaterObjs) > 0 {
		greatermin := node.min
		greatermin[node.dir] = node.splitVal
		node.greater = newBspNode(greaterObjs, node.layer+1, greatermin, node.max)
		bspNL = append(bspNL, node.greater)
	}
	return bspNL
}

// FindIntersections creates a sorted list of intersections between a ray and the facets in a bsptree if all is true
// if all is false FindIntersection returns when the first intersection is found.
func (node *BspNode) FindIntersections(ray *Ray, all bool, exceptions []int) (intersections IntersectionList, err error) {
	// find the portion of the ray that is inside the bounds of the target
	keepRay := NewRay(ray.GetRoot(), ray.GetEnd())
	for _, dir := range dirList {
		_, keepRay = ray.split(node.min[dir], dir)
		if keepRay == nil {
			return nil, nil
		}
		keepRay, _ = keepRay.split(node.max[dir], dir)
		if keepRay == nil {
			return nil, nil
		}
	}

	nrl := nodeRayList{newNodeRay(node, keepRay)}
	for i := 0; i < len(nrl); i++ {
		newIntersections, newNrl, err := nrl[i].node.findIntersections(nrl[i].ray, all, &exceptions)
		if err != nil {
			return intersections, err
		}
		if !all && len(intersections) > 0 {
			return intersections, nil
		}
		nrl = append(nrl, newNrl...)
		intersections = append(intersections, newIntersections...)
	}
	sort.Sort(intersections)
	return intersections, nil
}

func (node *BspNode) findIntersections(ray *Ray, all bool, exceptions *[]int) (intersections IntersectionList, nrl nodeRayList, err error) {

	// first check for intersections in this node
	if node.objs != nil {
		for _, obj := range node.objs {
			skip := false
			for _, exception := range *exceptions {
				if obj.id == exception {
					skip = true
					break
				}
			}
			if skip {
				continue
			}
			intersect, front, dist, loc, err := obj.TriIntersectB(ray)
			if err != nil {
				return intersections, nil, err
			}
			if intersect {
				intersections = append(intersections,
					&Intersection{Obj: obj, Front: front, Dist: dist, Loc: loc, Ray: ray})
				*exceptions = append(*exceptions, obj.id)
				if !all {
					return intersections, nil, nil
				}
			}
		}
	}

	if node.lesser == nil && node.greater == nil {
		return intersections, nil, nil
	}

	lray, gray := ray.split(node.splitVal, node.dir)
	if node.lesser != nil && lray != nil {
		nrl = append(nrl, newNodeRay(node.lesser, lray))
	}
	if node.greater != nil && gray != nil {
		nrl = append(nrl, newNodeRay(node.greater, gray))
	}

	return intersections, nrl, nil
}
