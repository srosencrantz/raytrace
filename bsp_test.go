/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package raytrace

import (
	"testing"
)

var nodes = [][3]float64{[3]float64{78.78124237060546875, -17.828105926513671875, 38.840362548828125},
	[3]float64{76.00118255615234375, -17.828105926513671875, 39.937480926513671875},
	[3]float64{78.39111328125, -18.02933502197265625, 38.399959564208984375},
	[3]float64{74.80915069580078125, -18.02933502197265625, 39.813541412353515625},
	[3]float64{73.2211151123046875, -17.828107833862304688, 41.03460693359375},
	[3]float64{71.22718048095703125, -18.02933502197265625, 41.2271270751953125},
	[3]float64{70.4410552978515625, -17.828107833862304688, 42.13169097900390625},
	[3]float64{67.6452178955078125, -18.02933502197265625, 42.640666961669921875},
	[3]float64{67.66098785400390625, -17.828107833862304688, 43.228816986083984375},
	[3]float64{78.78124237060546875, 17.828046798706054688, 38.840362548828125},
	[3]float64{78.78124237060546875, 21.614101409912109375, 38.840362548828125},
	[3]float64{78.39111328125, 18.02927398681640625, 38.399959564208984375},
	[3]float64{78.39111328125, 21.614101409912109375, 38.399959564208984375},
	[3]float64{78.39111328125, 25.198925018310546875, 38.399959564208984375},
	[3]float64{78.78124237060546875, 25.400152206420898438, 38.840362548828125},
	[3]float64{78.78124237060546875, -25.400213241577148438, 38.840362548828125},
	[3]float64{78.78124237060546875, -21.61415863037109375, 38.840362548828125},
	[3]float64{78.39111328125, -25.198986053466796875, 38.399959564208984375},
	[3]float64{78.39111328125, -21.61415863037109375, 38.399959564208984375}}

var facets = [][3]int{[3]int{1, 2, 3},
	[3]int{3, 2, 4},
	[3]int{4, 2, 5},
	[3]int{6, 5, 7},
	[3]int{8, 7, 9},
	[3]int{8, 6, 7},
	[3]int{4, 5, 6},
	[3]int{10, 11, 12},
	[3]int{12, 11, 13},
	[3]int{13, 11, 14},
	[3]int{14, 11, 15},
	[3]int{16, 17, 18},
	[3]int{18, 17, 19},
	[3]int{19, 17, 3},
	[3]int{3, 17, 1}}

var tris TriList

func init() {
	for i, f := range facets {
		tris = append(tris, NewTri(i+1, nodes[f[0]-1], nodes[f[1]-1], nodes[f[2]-1]))
	}
}

// func TestCreateBspTree(t *testing.T) {
// 	tree, err := CreateBspTree(1.0, tris)
// 	if err != nil {
// 		t.Error(err)
// 	}
// 	//	fmt.Println(tree.Sprint())

// 	numObjs := len(tree.objs)
// 	expectedObjs := 0
// 	if numObjs != expectedObjs {
// 		t.Errorf("TestCreateBspTree got root Objs = %d, expected Objs = %d", numObjs, expectedObjs)
// 	}
// 	numObjs = len(tree.greater.objs)
// 	expectedObjs = 0
// 	if numObjs != expectedObjs {
// 		t.Errorf("TestCreateBspTree got root.greater Objs = %d, expected Objs = %d", numObjs, expectedObjs)
// 	}
// 	numObjs = len(tree.lesser.objs)
// 	expectedObjs = 7
// 	if numObjs != expectedObjs {
// 		t.Errorf("TestCreateBspTree got root.lesser Objs = %d, expected Objs = %d", numObjs, expectedObjs)
// 	}
// 	numObjs = len(tree.greater.greater.objs)
// 	expectedObjs = 4
// 	if numObjs != expectedObjs {
// 		t.Errorf("TestCreateBspTree got root.greater.greater Objs = %d, expected Objs = %d", numObjs, expectedObjs)
// 	}
// 	numObjs = len(tree.greater.lesser.objs)
// 	expectedObjs = 6
// 	if numObjs != expectedObjs {
// 		t.Errorf("TestCreateBspTree got root.greater.lesser Objs = %d, expected Objs = %d", numObjs, expectedObjs)
// 	}
// }

func TestFindIntersections(t *testing.T) {
	tree, err := CreateBspTree(1.0, tris)
	if err != nil {
		t.Error(err)
	}
	ray := NewRay([3]float64{116.53, 228.15, 24.1}, [3]float64{41.03, -182.15, 53.1})
	intersections, err := tree.FindIntersections(ray, true, nil)
	//fmt.Println("intersections: ")
	//fmt.Println(intersections.String())
	if err != nil {
		t.Errorf("Got error: %v", err)
	}

	numIntersections := len(intersections)
	expectedIntersections := 2
	if numIntersections != expectedIntersections {
		t.Errorf("TestFindIntersections found %d intersections, expected %d", numIntersections, expectedIntersections)
	}

	firstFacetID := intersections[0].Obj.id
	expectedID := 10
	if firstFacetID != expectedID {
		t.Errorf("TestFindIntersections found the first intersecton on tri ID = %d, expected ID = %d", firstFacetID, expectedID)
	}

	secondFacetID := intersections[1].Obj.id
	expectedID = 4
	if secondFacetID != expectedID {
		t.Errorf("TestFindIntersections found the second intersecton on tri ID = %d, expected ID = %d", secondFacetID, expectedID)
	}

	firstFacetDist := intersections[0].Dist
	expectedDist := 209.972505
	if firstFacetDist > (expectedDist+1e-5) && firstFacetDist < (expectedDist-1e-5) {
		t.Errorf("TestFindIntersections found the first intersection at %f, expected %f", firstFacetDist, expectedDist)
	}

	secondFacetDist := intersections[0].Dist
	expectedDist = 250.821913
	if secondFacetDist > (expectedDist+1e-5) && secondFacetDist < (expectedDist-1e-5) {
		t.Errorf("TestFindIntersections found the second intersection at %f, expected %f", secondFacetDist, expectedDist)
	}
}
