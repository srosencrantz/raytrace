/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package raytrace

import (
	"bitbucket.org/srosencrantz/vect3d"
)

const debug = false

// contants for the GeoDesicPoints function
const x float64 = 0.525731112119133606
const z float64 = 0.850650808352039932

var vdata = [12][3]float64{
	{-x, 0.0, z}, {x, 0.0, z}, {-x, 0.0, -z}, {x, 0.0, -z},
	{0.0, z, x}, {0.0, z, -x}, {0.0, -z, x}, {0.0, -z, -x},
	{z, x, 0.0}, {-z, x, 0.0}, {z, -x, 0.0}, {-z, -x, 0.0}}
var tindices = [20][3]int{
	{0, 4, 1}, {0, 9, 4}, {9, 5, 4}, {4, 5, 8}, {4, 8, 1},
	{8, 10, 1}, {8, 3, 10}, {5, 3, 8}, {5, 2, 3}, {2, 7, 3},
	{7, 10, 3}, {7, 6, 10}, {7, 11, 6}, {11, 0, 6}, {0, 1, 6},
	{6, 1, 10}, {9, 0, 11}, {9, 11, 2}, {9, 2, 5}, {7, 2, 11}}

// GeoDesicPoints returns a list of points equally distributed over a sphere
// depth 0 returns 12 points, 1 returns 72 points, 2 returns 312 points, 3 returns 1272 points
// radius sets the radius of the points about the origin
func GeoDesicPoints(depth int, radius float64) (spherePoints [][3]float64) {
	spherePoints = make([][3]float64, 0)
	spherePoints = append(spherePoints, vdata[:]...)
	for i := 0; i < 20; i++ {
		geoSubdivide(vdata[tindices[i][0]], vdata[tindices[i][1]], vdata[tindices[i][2]], &spherePoints, depth)
	}
	for i := range spherePoints {
		spherePoints[i] = vect3d.ScalerMult(spherePoints[i], radius)
	}
	return spherePoints
}

func geoSubdivide(v1, v2, v3 [3]float64, spherePoints *[][3]float64, depth int) {
	if depth == 0 {
		return
	}
	v12 := vect3d.Normalize(vect3d.Add(v1, v2))
	v23 := vect3d.Normalize(vect3d.Add(v2, v3))
	v31 := vect3d.Normalize(vect3d.Add(v3, v1))
	*spherePoints = append(*spherePoints, [][3]float64{v12, v23, v31}...)
	geoSubdivide(v1, v12, v31, spherePoints, depth-1)
	geoSubdivide(v2, v23, v12, spherePoints, depth-1)
	geoSubdivide(v3, v31, v23, spherePoints, depth-1)
	geoSubdivide(v12, v23, v31, spherePoints, depth-1)
}
