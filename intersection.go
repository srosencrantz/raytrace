/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package raytrace

import (
	"fmt"
)

// Intersection describes the where a ray and a facet intersect
type Intersection struct {
	Obj   *Tri       // pointer to the tri hit
	Front bool       // did the ray hit the front (positive normal) side of the facet
	Dist  float64    // distance of the intersection from the root point of the ray
	Loc   [3]float64 // location in 3d of the inersection
	Ray   *Ray       // local ray segment that intersected the triangle
}

func (x *Intersection) String() string {
	return fmt.Sprintf("intersection: %v\n", *x) //fmt.Sprintf("Object: %s\nFront: %v\nDist: %f\nLoc %v\n\n", x.Obj.String(), x.Front, x.Dist, x.Loc)
}

// IntersectionList is a list of intersections that can be sorted from smallest Distance to largest
type IntersectionList []*Intersection

func (a IntersectionList) String() string {
	var result string
	for i, x := range a {
		result += fmt.Sprintf("%d: \n", i) + x.String()
	}
	return result
}

func (a IntersectionList) Len() int           { return len(a) }
func (a IntersectionList) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a IntersectionList) Less(i, j int) bool { return a[i].Dist < a[j].Dist }
