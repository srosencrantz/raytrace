/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package raytrace

import (
	"math"

	"bitbucket.org/srosencrantz/vect3d"
)

// Ray defines a ray or a segment of a ray with rootpt and an endpt.
// Distance from the RootPt to the actual root point (to allow for ray splitting)
// if the whole ray is represented by RootPt and EndPt this number would be 0.
type Ray struct {
	root           [3]float64 // The Root point of this ray segment.
	end            [3]float64 // The End point of this ray segment.
	dir            [3]float64 // direction vector of the ray
	ndir           [3]float64 // normalized dir
	distance       float64    // distance of this split ray's root from the actual root
	length         float64    // length of the ray segment
	minLoc, maxLoc [3]float64 // min / max  x, y, z
}

// NewRay creates a new ray struct
func NewRay(rootpt, endpt [3]float64) (newray *Ray) {
	newray = &Ray{root: rootpt, end: endpt}
	newray.setup()
	return newray
}

// NewSplitRay creats a new split ray struct that represents a portion of an
//  earlier ray with a distance from this ray's origin to the original rays origin
func newSplitRay(rootpt, endpt [3]float64, dist float64) (newray *Ray) {
	newray = &Ray{root: rootpt, end: endpt, distance: dist}
	newray.setup()
	return newray
}

func (ray *Ray) setup() {
	ray.dir = vect3d.Subtract(ray.end, ray.root)
	ray.length = vect3d.Magnitude(ray.dir)
	ray.ndir = vect3d.ScalerMult(ray.dir, 1/ray.length)
	for i := 0; i < 3; i++ {
		ray.minLoc[i] = math.Min(ray.root[i], ray.end[i])
		ray.maxLoc[i] = math.Max(ray.root[i], ray.end[i])
	}
}

func (ray *Ray) split(splitVal float64, dir DirectionType) (lesser, greater *Ray) {
	switch {
	case ray.minLoc[dir] < splitVal && ray.maxLoc[dir] <= splitVal:
		lesser = ray
		greater = nil

	case ray.minLoc[dir] >= splitVal && ray.maxLoc[dir] > splitVal:
		lesser = nil
		greater = ray

	default:
		// calc greater and lesser rays
		pvec := [3]float64{0, 0, 0}
		p0 := [3]float64{0, 0, 0}
		pvec[dir] += (1.0 + splitVal)
		p0[dir] += splitVal

		t := vect3d.DotProduct(vect3d.Subtract(p0, ray.root), pvec) / vect3d.DotProduct(ray.dir, pvec)
		divPoint := vect3d.Add(ray.root, vect3d.ScalerMult(ray.dir, t))
		newDist := ray.distance + vect3d.DistPt2Pt(ray.root, divPoint)

		if ray.root[dir] <= splitVal {
			lesser = newSplitRay(ray.root, divPoint, ray.distance)
			greater = newSplitRay(divPoint, ray.end, newDist)
		} else {
			lesser = newSplitRay(divPoint, ray.end, newDist)
			greater = newSplitRay(ray.root, divPoint, ray.distance)
		}
	}
	return lesser, greater
}

// GetRoot returns the root point of the ray
func (ray *Ray) GetRoot() [3]float64 {
	return ray.root
}

// GetEnd returns the end point of the ray
func (ray *Ray) GetEnd() [3]float64 {
	return ray.end
}

// GetDist return the distance of this split ray's root from the actual ray root
func (ray *Ray) GetDist() float64 {
	return ray.distance
}

// GetDir returns the ray's direction vector
func (ray *Ray) GetDir() [3]float64 {
	return ray.dir
}

// GetNDir returns the ray's normalized direction vector
func (ray *Ray) GetNDir() [3]float64 {
	return ray.ndir
}

// GetLength returns the ray's normalized direction vector
func (ray *Ray) GetLength() float64 {
	return ray.length
}

// MinLoc returns the min location on the dir axis
func (ray *Ray) MinLoc(dir DirectionType) float64 {
	return ray.minLoc[dir]
}

// MaxLoc returns the max location on the dir axis
func (ray *Ray) MaxLoc(dir DirectionType) float64 {
	return ray.maxLoc[dir]
}
