/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package raytrace

import (
	"fmt"
	"math"

	"bitbucket.org/srosencrantz/errcat"
	"bitbucket.org/srosencrantz/vect3d"
)

// Tri defines a triangle with three ordered points and an id
type Tri struct {
	id            int
	p             [3][3]float64 // list of three points
	normal        [3]float64    // triangle normal
	d             float64       // d value of the tri for triintersect
	i1, i2        int           // minor axis indexes for triintersect
	min, max, avg [3]float64    // min, max, and average coordinates from the 3 nodes
}

// TriList is a list of Tri pointers
type TriList []*Tri

// NewTri creates a new Tri and returns a pointer to it
func NewTri(ptid int, pt1, pt2, pt3 [3]float64) (newtri *Tri) {
	newtri = &Tri{id: ptid, p: [3][3]float64{pt1, pt2, pt3}}
	newtri.setup()
	return
}

// maxIndxLoc determines the position of the largest of 3 floats
func maxIndxLoc(x, y, z float64) (idx int) {
	switch {
	case x > y && x > z:
		idx = 0
	case y > x && y > z:
		idx = 1
	case z > x && z > y:
		idx = 2
	}
	return idx
}

// GetAvgPos returns the average position of the trilist
func (tpl TriList) GetAvgPos() (avgPos [3]float64) {
	for _, tri := range tpl {
		avgPos = vect3d.Add(avgPos, tri.AvgPos())
	}
	avgPos = vect3d.ScalerMult(avgPos, float64(1)/float64(len(tpl)))
	return avgPos
}

// GetMaxRadius returns the center of the triList and the
//  distance from the center to the most distant point
func (tpl TriList) GetMaxRadius() (center [3]float64, max float64) {
	center = tpl.GetAvgPos()
	for _, tri := range tpl {
		for _, point := range tri.p {
			dist := vect3d.DistPt2Pt(center, point)
			if dist > max {
				max = dist
			}
		}
	}
	return center, max
}

// setup sets the underlying parameters
func (tp *Tri) setup() {
	tp.normal = vect3d.CrossProduct(vect3d.Subtract(tp.p[1], tp.p[0]), vect3d.Subtract(tp.p[2], tp.p[0]))
	tp.d = vect3d.DotProduct(vect3d.ScalerMult(tp.p[0], -1.0), tp.normal)
	switch maxIndxLoc(math.Abs(tp.normal[0]), math.Abs(tp.normal[1]), math.Abs(tp.normal[2])) {
	case 0:
		tp.i1, tp.i2 = 1, 2
	case 1:
		tp.i1, tp.i2 = 0, 2
	case 2:
		tp.i1, tp.i2 = 0, 1
	}
	for i := 0; i < 3; i++ {
		tp.min[i] = math.Min(math.Min(tp.p[0][i], tp.p[1][i]), tp.p[2][i])
		tp.max[i] = math.Max(math.Max(tp.p[0][i], tp.p[1][i]), tp.p[2][i])
		tp.avg[i] = (tp.p[0][i] + tp.p[1][i] + tp.p[2][i]) / float64(3.0)
	}
}

// GetID returns the normal vector of the Tri
func (tp *Tri) GetID() int {
	return tp.id
}

// GetPt returns the Tri point specified by the index
func (tp *Tri) GetPt(i int) [3]float64 {
	return tp.p[i]
}

// GetNormal returns the normal vector of the Tri
func (tp *Tri) GetNormal() [3]float64 {
	return tp.normal
}

// GetD returns the d value of a triangle for use in TriIntersect
func (tp *Tri) GetD(normal [3]float64) float64 {
	return tp.d
}

// GetMinorAxisIdxs returns minor axis indexes for a triangle with the given normal for use in TriIntersect
func (tp *Tri) GetMinorAxisIdxs() (i1, i2 int) {
	return tp.i1, tp.i2
}

// String returns a string representation of the triangle
func (tp *Tri) String() string {
	return fmt.Sprintf("tri: %d", tp.id)
}

// AvgLoc returns the average position in the dir (x, y, or z) direction
func (tp *Tri) AvgLoc(dir DirectionType) float64 {
	return tp.avg[dir]
}

// AvgPos returns the average position of the tri
func (tp *Tri) AvgPos() [3]float64 {
	return tp.avg
}

// MinLoc returns the average position in the dir (x, y, or z) direction
func (tp *Tri) MinLoc(dir DirectionType) float64 {
	return tp.min[dir]
}

// MaxLoc returns the average position in the dir (x, y, or z) direction
func (tp *Tri) MaxLoc(dir DirectionType) float64 {
	return tp.max[dir]
}

// TriIntersectInfo assuming there is an intersection at the ray endpt
//   this function returns
//   whether the front (positive normal side) was hit,
//   the distance of the intersection from the origin,
//   and the intersection point
func (tp *Tri) TriIntersectInfo(ray *Ray) (front bool, distance float64, point [3]float64) {
	distance = ray.GetDist() + ray.GetLength()
	point = ray.GetEnd()
	front = vect3d.DotProduct(ray.GetNDir(), tp.normal) < 0
	return front, distance, point
}

// TriIntersect determines whether a line segment defined by rayOrigin and rayEnd intersects a triangle defined by the points p1, p2, p3. This algorithm was originally from an efficient ray-polygon intersection by Didier Badouel from the book Graphics Gems I */
func (tp *Tri) TriIntersect(ray *Ray) (intersect bool, t float64, front bool, distance float64, point [3]float64, err error) {
	nDotRayDir := vect3d.DotProduct(tp.normal, ray.GetDir())
	front = nDotRayDir < 0

	// check if is ray parallel to the triangle plane
	if nDotRayDir == 0.0 {
		if debug {
			err = errcat.AppendInfoStr(err, "ray is parallel to triangle plane")
		}
		return false, t, front, distance, point, err
	}

	t = float64(-1.0) * ((tp.d + vect3d.DotProduct(tp.normal, ray.GetRoot())) / nDotRayDir)

	// check if intersection is beyond the ends of the line segment
	if (t < 0.0) || (t > 1.0) {
		if debug {
			err = errcat.AppendInfoStr(err, "ray is beyond the ends of the line segment")
		}
		return false, t, front, distance, point, err
	}

	eot := vect3d.ScalerMult(ray.GetDir(), t)
	distance = vect3d.Magnitude(eot) + ray.GetDist()
	point = vect3d.Add(ray.GetRoot(), eot)
	u := [3]float64{point[tp.i1] - tp.p[0][tp.i1], tp.p[1][tp.i1] - tp.p[0][tp.i1], tp.p[2][tp.i1] - tp.p[0][tp.i1]}
	v := [3]float64{point[tp.i2] - tp.p[0][tp.i2], tp.p[1][tp.i2] - tp.p[0][tp.i2], tp.p[2][tp.i2] - tp.p[0][tp.i2]}

	if u[1] == 0.0 {
		beta := u[0] / u[2]
		if (beta >= 0.0) && (beta <= 1.0) {
			alpha := (v[0] - beta*v[2]) / v[1]
			intersect = (alpha >= 0.0) && ((alpha + beta) <= 1.0)
			err = errcat.AppendInfoStr(err, fmt.Sprintf("u[1] == 0.0, alpha = %f, beta = %f", alpha, beta))
			return intersect, t, front, distance, point, err

		}
		err = errcat.AppendInfoStr(err, fmt.Sprintf("u[1] == %f, beta = %f", u[1], beta))

	} else {
		beta := (v[0]*u[1] - u[0]*v[1]) / (v[2]*u[1] - u[2]*v[1])
		if (beta >= 0.0) && (beta <= 1.0) {
			alpha := (u[0] - beta*u[2]) / u[1]
			intersect = (alpha >= 0.0) && ((alpha + beta) <= 1.0)
			err = errcat.AppendInfoStr(err, fmt.Sprintf("u[1] == %f, alpha = %f, beta = %f", u[1], alpha, beta))
			return intersect, t, front, distance, point, err
		}
		err = errcat.AppendInfoStr(err, fmt.Sprintf("u[1] == %f, beta = %f, u = %v, v = %v", u[1], beta, u, v))
	}
	return false, t, front, distance, point, err
}

// TriIntersectB determines whether a ray segment intersects a triangle. This algorithm was originally from Moller-Trumbore intersection algorithm */
func (tp *Tri) TriIntersectB(ray *Ray) (intersect bool, front bool, distance float64, point [3]float64, err error) {
	epsilon := 0.000001
	edge1 := vect3d.Subtract(tp.p[1], tp.p[0])
	edge2 := vect3d.Subtract(tp.p[2], tp.p[0])

	h := vect3d.CrossProduct(ray.GetDir(), edge2)
	a := vect3d.DotProduct(edge1, h)

	if (a > -epsilon) && (a < epsilon) {
		return false, front, distance, point, nil
	}

	f := 1 / a
	s := vect3d.Subtract(ray.GetRoot(), tp.p[0])
	u := f * vect3d.DotProduct(s, h)

	if (u < 0.0) || (u > 1.0) {
		return false, front, distance, point, nil
	}

	q := vect3d.CrossProduct(s, edge1)
	v := f * vect3d.DotProduct(ray.GetDir(), q)

	if (v < 0.0) || (u+v > 1.0) {
		return false, front, distance, point, nil
	}

	t := f * vect3d.DotProduct(edge2, q)
	if t >= 0.0 && t <= 1.0 {
		eot := vect3d.ScalerMult(ray.GetDir(), t)
		distance = vect3d.Magnitude(eot) + ray.GetDist()
		point = vect3d.Add(ray.GetRoot(), eot)
		front = a > 0
		return true, front, distance, point, nil
	}
	return false, front, distance, point, nil
}

// GetSamplePoints creates a list of sample points on a triangle
func (tp *Tri) GetSamplePoints(min float64) (samplePoints [][3]float64) {
	samplePoints = make([][3]float64, 0)

	// move the 3 points in from the verticies just a little bit so they're not right on the edge
	center := vect3d.ScalerMult(vect3d.Add(tp.p[2], vect3d.Add(tp.p[0], tp.p[1])), 0.33333)
	a := vect3d.Add(tp.p[0], vect3d.ScalerMult(vect3d.Subtract(center, tp.p[0]), 0.01))
	b := vect3d.Add(tp.p[1], vect3d.ScalerMult(vect3d.Subtract(center, tp.p[1]), 0.01))
	c := vect3d.Add(tp.p[2], vect3d.ScalerMult(vect3d.Subtract(center, tp.p[2]), 0.01))

	// get the points a, b, and c such that bc is the longest side
	abV := vect3d.Subtract(b, a)
	bcV := vect3d.Subtract(c, b)
	caV := vect3d.Subtract(a, c)

	ab := vect3d.Magnitude(abV)
	bc := vect3d.Magnitude(bcV)
	ca := vect3d.Magnitude(caV)

	switch {
	case ab > bc && ab > ca:
		a, b, c = c, a, b
		ab, bc, ca = ca, ab, bc
		abV, bcV, caV = caV, abV, bcV

	case ca > bc && ca > ab:
		a, b, c = b, c, a
		ab, bc, ca = bc, ca, ab
		abV, bcV, caV = bcV, caV, abV
	}

	// add the corner points to the list
	samplePoints = append(samplePoints, a)
	samplePoints = append(samplePoints, b)
	samplePoints = append(samplePoints, c)

	// if the longest sid is less than min we're done
	if bc < min {
		return
	}

	// add points for bc line
	numSeg := int((bc / min) + 0.5)
	segFrac := float64(1) / float64(numSeg)
	for i := 1; i < numSeg; i++ {
		samplePoints = append(samplePoints, vect3d.Add(b, vect3d.ScalerMult(bcV, segFrac*float64(i))))
	}

	// stop if adist is less than min
	adist := vect3d.DistPt2Line(b, vect3d.Normalize(bcV), a)
	if adist < min {
		return samplePoints
	}

	// loop over additional rows making points
	numRows := int((adist / min) + 0.5)
	//	rowHeight := adist / float64(numRows)
	baV := vect3d.ScalerMult(abV, -1)
	//abc := vect3d.GetAngle(baV, bcV)
	//bca := vect3d.GetAngle(vect3d.ScalerMult(bcV, -1), caV)
	//baStep := rowHeight / math.Sin(abc)
	baFrac := float64(1) / float64(numRows) // baStep / ab
	//caStep := rowHeight / math.Sin(bca)
	caFrac := float64(1) / float64(numRows) //caStep / ca
	//	println("ab: ", ab, " baStep: ", baStep, " rowHeight: ", rowHeight, " angle: ", abc*180/3.1415926, " sin: ", math.Sin(abc), " frac: ", baFrac, " adist: ", adist)
	for i := 1; i < numRows; i++ {
		d := vect3d.Add(b, vect3d.ScalerMult(baV, baFrac*float64(i)))
		e := vect3d.Add(c, vect3d.ScalerMult(caV, caFrac*float64(i)))
		deV := vect3d.Subtract(e, d)
		de := vect3d.Magnitude(deV)
		numSeg = int((de / min) + 0.5)
		segFrac = float64(1) / float64(numSeg)
		for j := 0; j < numSeg+1; j++ {
			samplePoints = append(samplePoints, vect3d.Add(d, vect3d.ScalerMult(deV, segFrac*float64(j))))
		}
	}

	return samplePoints
}
