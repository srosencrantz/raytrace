/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package raytrace

import (
	"fmt"
	"testing"

	"bitbucket.org/srosencrantz/vect3d"
)

func TestTriIntersect(t *testing.T) {
	tri := NewTri(1, [3]float64{1.0, 1.0, -1.0}, [3]float64{1.0, -1.0, -1.0}, [3]float64{1.0, 0.0, 1.0})
	ray := NewRay([3]float64{0.0, 0.0, 0.0}, [3]float64{2.0, 0.0, 0.0})

	intersect, front, distance, point, err := tri.TriIntersectB(ray)
	if !intersect {
		t.Errorf("TriIntersect said the Ray didn't intersect the triangle and it should have.")
	}

	if err != nil {
		t.Errorf("TriIntersect got err: %s expected none.", err)
	}

	if distance != 1.0 {
		t.Errorf("TriIntersect got distance = %f, expected distance = 1.0", distance)
	}

	if !front {
		t.Errorf("TriIntersect said it hit the back it actually hit the front")
	}

	if point != [3]float64{1.0, 0.0, 0.0} {
		t.Errorf("Triintersect got intersection point %v it should be %v", point, [3]float64{1.0, 0.0, 0.0})
	}
}

func TestTriIntersect2(t *testing.T) {

	// should hit
	tria := NewTri(1, [3]float64{8.28017540e+01, 4.04507430e+01, 5.02923120e+01},
		[3]float64{4.45096170e+01, 5.64766940e+01, 4.00145150e+01},
		[3]float64{4.28987690e+01, 4.87300400e+01, 5.63350600e+01})

	// don't want to hit
	trib := NewTri(2, [3]float64{4.44087410e+01, 5.58848210e+01, 4.03898160e+01},
		[3]float64{4.28656270e+01, 4.84639640e+01, 5.60239710e+01},
		[3]float64{8.29235650e+01, 4.01657920e+01, 4.99313510e+01})

	//trib = [4.44087410e+01, 5.58848210e+01, 4.03898160e+01; 4.28656270e+01, 4.84639640e+01, 5.60239710e+01; 8.29235650e+01, 4.01657920e+01, 4.99313510e+01]

	trilist := TriList{tria, trib}

	tree, err := CreateBspTree(2.0, trilist)
	if err != nil {
		t.Errorf("CreateBspTree got err: %s expected none.", err)
	}

	//fmt.Println(tree.Sprint())

	ray := NewRay([3]float64{5082.541097919663, 5040.531755638084, 7121.346002147412},
		[3]float64{82.54109791966201, 40.5317556380841, 50.2781902819371})

	intersections, err := tree.FindIntersections(ray, false, nil)
	if err != nil {
		t.Errorf("TriIntersect got err: %s expected none.", err)
	}

	if intersections == nil {
		fmt.Println("ray: ", *ray)
		fmt.Println("tria: ", *tria)
		fmt.Println("trib: ", *trib)
		t.Errorf("TriIntersect said the Ray didn't intersect the triangle and it should have.")
	}

	//fmt.Println(intersections)

	//fmt.Println("Ray: ", intersections[0].Ray)
	if len(intersections) > 1 {
		t.Errorf("Expected 1 intersection found %d intersections", len(intersections))
	}

	expectedDist := vect3d.DistPt2Pt(ray.GetRoot(), ray.GetEnd())
	if !(intersections[0].Dist > expectedDist-0.0001 && intersections[0].Dist < expectedDist+0.0001) {
		t.Errorf("TriIntersect got distance = %f, expected distance = %f", intersections[0].Dist, expectedDist)
	}

	if !intersections[0].Front {
		t.Errorf("TriIntersect said it hit the back it actually hit the front")
	}

	if vect3d.DistPt2Pt(intersections[0].Loc, ray.GetEnd()) > 0.0001 {
		t.Errorf("Triintersect got intersections point %v it should be %v", intersections[0].Loc, ray.GetEnd())
	}
}

func TestTriIntersect3(t *testing.T) {

	// should hit
	tria := NewTri(1, [3]float64{8.28017540e+01, 4.04507430e+01, 5.02923120e+01},
		[3]float64{4.45096170e+01, 5.64766940e+01, 4.00145150e+01},
		[3]float64{4.28987690e+01, 4.87300400e+01, 5.63350600e+01})

	// don't want to hit
	trib := NewTri(2, [3]float64{4.44087410e+01, 5.58848210e+01, 4.03898160e+01},
		[3]float64{4.28656270e+01, 4.84639640e+01, 5.60239710e+01},
		[3]float64{8.29235650e+01, 4.01657920e+01, 4.99313510e+01})

	//trib = [4.44087410e+01, 5.58848210e+01, 4.03898160e+01; 4.28656270e+01, 4.84639640e+01, 5.60239710e+01; 8.29235650e+01, 4.01657920e+01, 4.99313510e+01]

	trilist := TriList{tria, trib}

	tree, err := CreateBspTree(2.0, trilist)
	if err != nil {
		t.Errorf("CreateBspTree got err: %s expected none.", err)
	}

	//fmt.Println(tree.Sprint())

	ray := NewRay([3]float64{5082.541097919663, 5040.531755638084, 7121.346002147412},
		[3]float64{82.54109791966201, 40.5317556380841, 50.2781902819371})

	intersections, err := tree.FindIntersections(ray, false, []int{1})
	if err != nil {
		t.Errorf("TriIntersect got err: %s expected none.", err)
	}

	if intersections != nil {
		fmt.Println("ray: ", *ray)
		fmt.Println("tria: ", *tria)
		fmt.Println("trib: ", *trib)
		fmt.Println(intersections)
		t.Errorf("TriIntersect said the Ray intersected a triangle and it shouldn't have.")
	}

}

func samplePointsTest(t *testing.T, a1, a2, a3 [3]float64, min float64, expected int) {
	tri := NewTri(1, a1, a2, a3)
	sPoints := tri.GetSamplePoints(min)
	if len(sPoints) != expected {
		fmt.Printf("triangle = [%15.10f %15.10f %15.10f;%15.10f %15.10f %15.10f;%15.10f %15.10f %15.10f];",
			a1[0], a1[1], a1[2], a2[0], a2[1], a2[2], a3[0], a3[1], a3[2])
		fmt.Printf("samplepoints = [")
		for i, point := range sPoints {
			fmt.Printf("%15.10f %15.10f %15.10f", point[0], point[1], point[2])
			if i < len(sPoints)-1 {
				fmt.Printf(";")
			}
		}
		fmt.Printf("]; plot3(triangle(:,1), triangle(:,2), triangle(:,3), 'g'); hold on; plot3(samplepoints(:,1), samplepoints(:,2), samplepoints(:,3), '-o'); axis equal; hold off")
		t.Errorf("TestTriSamplePoints got %d it should be %d", len(sPoints), expected)
	}
}

func TestTriSamplePoints(t *testing.T) {
	min := 1.0

	a1 := [3]float64{0.0, 0.0, 0.0}
	a2 := [3]float64{0.5, 0.0, 0.0}
	a3 := [3]float64{0.0, 0.5, 0.0}
	expected := 3
	samplePointsTest(t, a1, a2, a3, min, expected)

	a1 = [3]float64{-138.303192138671875, 55.0151519775390625, 75.86029815673828125}
	a3 = [3]float64{-124.30319976806640625, 55.749988555908203125, 75.75046539306640625}
	a2 = [3]float64{-138.303192138671875, 54.749988555908203125, 75.75046539306640625}
	expected = 16
	samplePointsTest(t, a1, a2, a3, min, expected)

	a1 = [3]float64{-143.2880401611328125, -53.50000762939453125, 89.8603057861328125}
	a2 = [3]float64{-143.1782073974609375, 49.499988555908203125, 90.12546539306640625}
	a3 = [3]float64{-143.2880401611328125, 49.499988555908203125, 89.8603057861328125}
	expected = 104
	samplePointsTest(t, a1, a2, a3, min, expected)

	a1 = [3]float64{0, 0, 0}
	a2 = [3]float64{3, 0, 0}
	a3 = [3]float64{1.5, 3, 0}
	expected = 10
	samplePointsTest(t, a1, a2, a3, min, expected)

	a1 = [3]float64{0, 0, 0}
	a2 = [3]float64{15, 0, 0}
	a3 = [3]float64{14, 3, 0}
	expected = 34
	samplePointsTest(t, a1, a2, a3, min, expected)

}
